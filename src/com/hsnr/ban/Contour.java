package com.hsnr.ban;

import java.util.ArrayList;
import java.util.Iterator;

import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.*;
import com.googlecode.javacv.cpp.opencv_highgui.*;
import com.googlecode.javacv.cpp.opencv_imgproc.*;

import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;

public class Contour {
	private ArrayList<CvPoint> allPoints;
//	private ArrayList<CvPoint> Points_UP;
//	private ArrayList<CvPoint> Points_BOTTOM;
	private ArrayList<CvPoint> modelPoints;
	private int maxX;

	public Contour(CvSeq seq) {
		this.allPoints = new ArrayList<CvPoint>();
		this.modelPoints = new ArrayList<CvPoint>();
		int left = 0, right = 0, left_value = Integer.MAX_VALUE, right_value = 0;
		// gehe ueber alle Punkte von einem Kontur
		for (int i = 0; i < seq.total(); i++) {
			CvPoint p = new CvPoint(cvGetSeqElem(seq, i));
			// CvPoint p=new CvPoint(seq.first().data().position(i)); //so gets
			// auch
			if (p.x() < left_value) { // suche Punkt mit der kleinste x-Koord
				left = i;
				left_value = p.x();
			}
			if (p.x() > right_value) {// suche Punkt mit der groesste x-Koord
				right = i;
				right_value = p.x();
			}
		}
		// System.out.println("min-x:" +left_value+ " max-x:" + right_value
		// +" ");
		this.maxX = right - left;
		int j = left;
		// Folge umkopieren und an Stelle 0 den linkesten Wert setzen:
		for (int i = 0, ii = seq.total(); i < ii; i++) {
			CvPoint p = new CvPoint(cvGetSeqElem(seq, j));
			allPoints.add(p);
			// Linken Punkt, also kleinsten Wert suchen:
			j += 1;
			if (j == ii) {
				j = 0;
			}
		}

	}

	public int getPosMinX() {
		return 0;
	}

	public int getPosMaxX() {
		return this.maxX;
	}

	public int getMinXValue() {
		return this.getPointAt(0).x();
	}

	public int getMaxXValue() {
		return this.getPointAt(this.maxX).x();
	}

	public CvPoint getModelPointAt(int position) {
		return this.modelPoints.get(position);
	}

	public CvPoint getPointAt(int position) {
		return this.allPoints.get(position);
	}
	public int size() {
		return this.allPoints.size();

	}
	public int sizeModelPoints() {
		return this.modelPoints.size();

	}

	public void createModelPoints(int totalPointsCount) {

		int mu = 0, mb = 0;
		ArrayList<CvPoint> Points_UP = new ArrayList<CvPoint>();
		ArrayList<CvPoint> Points_BOTTOM = new ArrayList<CvPoint>();
		Points_UP.add(this.getPointAt(this.getPosMinX()));
		Points_BOTTOM.add(this.getPointAt(this.getPosMaxX()));
		int d = (this.getMaxXValue() - this.getMinXValue())
				/ ((totalPointsCount - 2) / 2 + 1);

		int i = 0, j = this.maxX;
		int pos = 0;

		while (pos < ((totalPointsCount - 2) / 2)) {
			while (this.getPointAt(i).x() < d * (pos + 1) + this.getMinXValue()
					&& i < this.size()) {
				i++;
			}
			if (d - this.getPointAt(i - 1).x() < this.getPointAt(i).x() - d) {
				mu = i - 1;
				i++;
			} else {
				mu = i;
				i = +2;
			}

			if (i >= this.size()) {
				mu = i;
			}

			if (j >= this.size() - 1) {
				mb = j - 1;
			} else {
				while (this.getPointAt(j).x() > this.getMaxXValue() - d
						* (pos + 1)
						&& j < this.size()) {
					j++;
				}
				if (this.getPointAt(j).x() - d > d - this.getPointAt(j - 1).x()) {
					mb = j - 1;
					j++;
				} else {
					mb = j;
					j = +2;
				}
			}
			Points_UP.add(this.getPointAt(mu));
			Points_BOTTOM.add(this.getPointAt(mb));
			pos++;
		}
		modelPoints.clear();
		modelPoints.addAll(Points_UP);
		modelPoints.addAll(Points_BOTTOM);
	}
	
	public CvPoint berechneContourZentrum(){
		CvPoint result = new CvPoint(0,0);

		for(int i=0; i < modelPoints.size(); i++){
			result.put(result.x()+ modelPoints.get(i).x(),result.y()+ modelPoints.get(i).y());
		}
		result.x(result.x() / modelPoints.size());
		result.y(result.y() / modelPoints.size());

		return result;
	}
//
//	public CvPoint berechneModellRotation(){
//		// result.x = a
//		// result.y = b
//		CvPoint result;
//
//		for(int i=0; i < modellPunkte.size(); i++){
//			for(int j=0; j < modellPunkte[i].size(); j++){
//				result.x = modellPunkte[i][j].x;
//				result.y = modellPunkte[i][j].y;
//			}
//		}
//
//		return result;
//	}
//
	public float S_x(){
		float result = 0;

		for(int i=0; i < modelPoints.size(); i++){
				result += modelPoints.get(i).x();
			}
		result=result / modelPoints.size();
		return result;
	}
	public float S_xx(){
		float result = 0;

		for(int i=0; i < modelPoints.size(); i++){
				result += (modelPoints.get(i).x()*modelPoints.get(i).x());
			}
		result=result / modelPoints.size();
		return result;
	}
	
	public float S_y(){
		float result = 0;

		for(int i=0; i < modelPoints.size(); i++){
				result += modelPoints.get(i).y();
			}
		result=result / modelPoints.size();
		return result;
	}
	public float S_yy(){
		float result = 0;

		for(int i=0; i < modelPoints.size(); i++){
				result += (modelPoints.get(i).y()*modelPoints.get(i).y());
			}
		result=result / modelPoints.size();
		return result;
	}

	public float S_xy(){
		float result = 0;

		for(int i=0; i < modelPoints.size(); i++){
			result += modelPoints.get(i).x() * modelPoints.get(i).y();
		}
		result = result / modelPoints.size();

		return result;
	}




	public ArrayList<CvPoint> angleichen(CvPoint diff){
		ArrayList<CvPoint> normPoints = new ArrayList<CvPoint>();
		//erstelle Array mit Normalisierten Punkten
		for(int i=0; i < modelPoints.size(); i++){
			normPoints.add(new CvPoint(modelPoints.get(i).x()-diff.x(),modelPoints.get(i).y()-diff.y()));
		}
		return normPoints;
	}


	@Override
	public String toString() {
		Iterator<CvPoint> i = allPoints.iterator();
		String points = "MinX:" + this.getPointAt(this.getPosMinX()).x()
				+ ",At:" + this.getPosMinX() + " MaxX:"
				+ this.getPointAt(this.getPosMaxX()).x() + ",At:"
				+ this.getPosMaxX() + ", Points: ";
		while (i.hasNext()) {
			CvPoint p = i.next();
			// points += "x:"+ p.x() + " y:" + p.y() + " ";
			points += "(" + p.x() + "," + p.y() + ") ";
		}
		return points;

	}
}
