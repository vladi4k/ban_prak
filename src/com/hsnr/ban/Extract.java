package com.hsnr.ban;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacpp.Pointer;
import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_core.*;
import com.googlecode.javacv.cpp.opencv_highgui.*;
import com.googlecode.javacv.cpp.opencv_imgproc.*;
import com.googlecode.javacv.cpp.opencv_ml.CvVectors;

import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;

public class Extract {
	String fileName;
	CvMat image;
	CvSeq contour_seq;
	int countersCount;
	ArrayList<Contour> contours;
	
	public Extract(String fileName) {
		super();
		this.fileName = fileName;
		this.image=cvLoadImageM(fileName,0);
		//this.contours = CvMemStorage.create();
		this.contours = new ArrayList<Contour>();
		this.contour_seq = new CvSeq() ;
		CvMemStorage contours = CvMemStorage.create();

		this.countersCount = cvFindContours(this.image, contours , this.contour_seq, Loader.sizeof(CvContour.class) , CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		
		CvSeq seq=this.contour_seq;
		while(seq!=null)
		{
			this.contours.add(new Contour(seq)); //liest cvContour aus und wandelt ihn in ein Java-Object um
			seq=seq.h_next(); //ab zu dem naechsten Kontur
		}
	}
	public void showContoures()
	{
		CvMat drawing =CvMat.create(this.image.rows(),this.image.cols(),8,3);
		cvZero(drawing);
		CvSeq seq=this.contour_seq;
		while(seq!=null)
		{
			cvDrawContours(drawing, seq, cvScalar(255, 255, 255, 0), cvScalar(255, 255, 255, 0), -1, CV_FILLED, 8);
			seq=seq.h_next();
		}
		CanvasFrame canvas = new CanvasFrame("showContoures");
		canvas.showImage(drawing.asIplImage());
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		cvWaitKey();
		
	}
	public void splitContours(int totalPointsCount)
	{
		Iterator<Contour> i = this.contours.iterator();
		while (i.hasNext()) {
			i.next().createModelPoints(totalPointsCount);	
		}
	}
	public void showSplitedContoures()
	{
		CvMat drawing =CvMat.create(image.rows(),image.cols(),image.depth(),image.channels());
//		cvZero(drawing);
		cvCopy(image, drawing);
		Iterator<Contour> it = this.contours.iterator();
		while (it.hasNext()) {
			Contour c = it.next();
			for (int b = 0; b < c.sizeModelPoints(); b++) {
				cvCircle(drawing, c.getModelPointAt(b), 1,cvScalarAll(127) , 2, 1, 0);
			}
//			CvPoint p=c.berechneModellZentrum();
//			cvCircle(drawing, p, 1,cvScalarAll(200) , 2, 1, 0);
//			System.out.println(c);
//			System.out.println("mitte->x:"+ p.x() + " y:" + p.y());
		}
		
		CanvasFrame canvas = new CanvasFrame("showSplitedContoures");
		canvas.showImage(drawing.asIplImage());
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		cvWaitKey();
		
	}
//	private float distance_between_2Points(Point p1, Point p2){
//		float x = abs(p2->x - p1->x);
//		float y = abs(p2->y - p1->y);
//
//		return sqrt(pow(x,2)+pow(y,2));
//	}

//	public int S_yy_strich(vector< vector < Point > > modellPunkte_strich){
//	int result = 0;
//	int count = 0;
//
//	for(int i=0; i < modellPunkte.size(); i++){
//		for(int j=0; j < modellPunkte[i].size(); j++){
//			result += modellPunkte[i][j].y * modellPunkte_strich[i][j].y;
//			count++;
//		}
//	}
//	result = result / count;
//
//	return result;
//}
//
//public int S_yx_strich(vector< vector < Point > > modellPunkte_strich){
//	int result = 0;
//	int count = 0;
//
//	for(int i=0; i < modellPunkte.size(); i++){
//		for(int j=0; j < modellPunkte[i].size(); j++){
//			result += modellPunkte[i][j].y * modellPunkte_strich[i][j].x;
//			count++;
//		}
//	}
//	result = result / count;
//
//	return result;
//}
//
//public int S_xy_strich(vector< vector < Point > > modellPunkte_strich){
//	int result = 0;
//	int count = 0;
//
//	for(int i=0; i < modellPunkte.size(); i++){
//		for(int j=0; j < modellPunkte[i].size(); j++){
//			result += modellPunkte[i][j].x * modellPunkte_strich[i][j].y;
//			count++;
//		}
//	}
//	result = result / count;
//
//	return result;
//}
//

	@Override
	public String toString() {
		Iterator<Contour> i = this.contours.iterator();
		String contours = "";
		while (i.hasNext()) {
			Contour c = i.next();
			contours += c.toString() + "\n";
		}
		return contours;

	}
}
